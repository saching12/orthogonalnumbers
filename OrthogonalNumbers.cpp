#include<iostream>
#include <stdlib.h>
#include<iomanip>
#include<bitset>
#include<vector>
#include "OrthogonalNumbers.h"
#include "RandomNumberSet.h"

using namespace std;


OrthogonalNumbers::OrthogonalNumbers(){
    
    maxSize = 39;
    difference = 4;
    size_of_set = 5;
    srandom(time(0));

}


OrthogonalNumbers::OrthogonalNumbers(int n, int c, int d){
    
    maxSize = n;
    size_of_set = c;
    difference = d;

}


bool OrthogonalNumbers::generate(int numbersOfRandomNumberSetsToGenerate){
    int random_num;
    vector<int> set_of_num;
    
    if(((difference *numbersOfRandomNumberSetsToGenerate) + size_of_set - difference) <= maxSize){
        
        for(int i = 0; i < numbersOfRandomNumberSetsToGenerate; i++){
            RandomNumberSet bit_set_number(maxSize);
            cout << "Set Number"<< (i+1) << endl;
            
            for(int j = 0; j < size_of_set; j++){
                cout << "size_of_set loop "<< (j+1) << endl;
                random_num = (random() % maxSize) + 1;
                if(!bit_set_number.isset(random_num)){
                if(list_of_set.size() == 0)
                {
                    bit_set_number.set(random_num);
                    set_of_num.push_back(random_num);
                    
                }
                else{
                    bit_set_number.set(random_num);
                    if(bit_set_number.size() > (size_of_set - difference)){
                        int k = 0;
                        while(k < random_set.size()){
                            cout << bit_set_number.size()<< endl;
                            if(k == 0){
                                if((bit_set_number - random_set[k]) >= difference)
                                {
                                    k++;
                                }
                                else{
                                    bit_set_number.unset(random_num);
                                    random_num = (random() % maxSize) + 1;
                                    bit_set_number.set(random_num);
                                }
                                
                            }
                            else{
                                if((bit_set_number - random_set[k]) >= difference)
                                {
                                    k++;
                                }
                                else{
                                    bit_set_number.unset(random_num);
                                    random_num = (random() % maxSize) + 1;
                                    bit_set_number.set(random_num);
                                    k = 0;
                                }
                            }
                        }
                    }
                }
            }
            else
                j--;
            }
            random_set.push_back(bit_set_number);

            list_of_set.push_back(set_of_num);
            set_of_num.clear();
        
        }
        return true;
    }
    else{
        cerr << " total number of Random numbers required to make "
             << numbersOfRandomNumberSetsToGenerate <<  " sets of size "
             << size_of_set << endl
             << " with distance " << difference << " is more than provided N" <<endl;
        return false;
    }
    
}



bool OrthogonalNumbers::reset(){
    
    random_set.clear();
    
    return true;
}


ostream& operator<<(ostream& os,const OrthogonalNumbers& orth_num){
    
    for(int i = 0;i<(int)orth_num.random_set.size();i++){
            os << orth_num.random_set[i] << endl;
    }
    return os;
}


void check_distance(OrthogonalNumbers& orth_num){
    
    for(int i = 0; i < orth_num.random_set.size();i++){
        for(int j = i + 1; j < orth_num.random_set.size();j++){
            cout << "distance" << (orth_num.random_set[i] - orth_num.random_set[j]) << endl;
        
        }
    
    }
}


