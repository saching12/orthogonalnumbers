#ifndef ORTHOGONALNUMBERS_H
#define ORTHOGONALNUMBERS_H
#include<bitset>
#include "RandomNumberSet.h"

using namespace std;



class OrthogonalNumbers{
    
    friend ostream& operator<<(ostream& os,const OrthogonalNumbers& bit);
    friend void check_distance(OrthogonalNumbers& orth_num);
public:
    OrthogonalNumbers();
    
    OrthogonalNumbers(int n, int c, int d);
    
    bool generate(int numbersOfRandomNumberSetsToGenerate);
    
    bool reset();
    
    
    
    
private:
    int maxSize, difference, size_of_set;
    vector<RandomNumberSet>  random_set;
    vector<vector<int>> list_of_set;
    
    
    
    
    
};

#endif
