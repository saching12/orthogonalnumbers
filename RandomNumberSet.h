#ifndef RANDOMNUMBERSET_H
#define RANDOMNUMBERSET_H
#include<bitset>


using namespace std;

class RandomNumberSet{
    friend ostream& operator<<(ostream& os,const RandomNumberSet& bit);
public:
    RandomNumberSet(); //enter the max number up upto which we will set the bitset (less than 150)
    
    RandomNumberSet(int n); //enter the max number up upto which we will set the bitset (less than 150)
    
    bool set(int i); //set the corresponding bit of integer i to 1
    
    bool unset(int i); //set the corresponding bit of integer i to 0
    
    bool isset(int i); //check if the corresponding bit of integer i is set to 1
    
    void reset(); //clear the bitset
    
    int size(); //return the number of bits are set to 1
    
    int operator-(RandomNumberSet& bit);
    
private:
    bitset<150>  bit_number;
    int maxSize;
    
    
    
};


#endif
