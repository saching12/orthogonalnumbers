#include<iostream>
#include <stdlib.h>
#include<iomanip>
#include<bitset>
#include<vector>
#include "RandomNumberSet.h"
using namespace std;


RandomNumberSet::RandomNumberSet(){
        maxSize = 39;
}


RandomNumberSet::RandomNumberSet(int n){
        
        if(n < 1){
            cerr << "Entered range is less than 1";
            exit(0);
        }
        else{
               maxSize = n;
           }
}
    

bool RandomNumberSet::set(int i){
    
    if((0 < i) && (i <= maxSize)){
        
            if(bit_number[i] == 1){
                return false;
            }
            else{
                bit_number.set(i,1);
                return true;
            }
        
    }
    else{
        cerr << "inout I is out of the range!!! Try again --- "<< i <<endl;
        exit(0);
            }
    
}


bool RandomNumberSet::unset(int i){
    
    
            bit_number.set(i,0);
            return true;

    
}


bool RandomNumberSet::isset(int i){
    

        if(bit_number[i] == 1)
            return true;
        else
            
            return false;
    
        
   
}


void RandomNumberSet::reset(){
    bit_number.reset();
}


int RandomNumberSet::size(){
    
    return bit_number.count();
    
}


ostream& operator<<(ostream& os,const RandomNumberSet& bit){

    for(int i = 0;i<(int)bit.bit_number.size();i++){
        if(bit.bit_number.test(i))
            os << setfill(' ')<< setw(3) <<i << " ";
    }
    return os;
}


int RandomNumberSet::operator-(RandomNumberSet& bit){
    
    int x = 0;
    if(bit_number.count() > bit.size())
        x =bit_number.count();
    else
        x = bit.size();
    
    
    return (x - (bit_number&bit.bit_number).count());
    
}
